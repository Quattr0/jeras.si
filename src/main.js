import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { routes } from './routes.js'
import VueObserveVisibility from 'vue-observe-visibility'
import VueSmoothScroll from 'vue-smoothscroll'
import VueParticles from 'vue-particles'

Vue.use(VueRouter);
Vue.use(VueObserveVisibility);
Vue.use(VueSmoothScroll);
Vue.use(VueParticles)

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        return {x: 0, y: 0};
    }
});

new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
