import Home from './components/pages/Home.vue';

export const routes = [
    { path: '/', component: Home, name: 'home' },
    { path: '*', redirect: '/' }
];
